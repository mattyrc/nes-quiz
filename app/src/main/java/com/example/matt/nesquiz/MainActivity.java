// Created by Matt Cates (cates044@gmail.com) for Udacity

package com.example.matt.nesquiz;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;

public class MainActivity extends AppCompatActivity {

    // Initial score
    private int score = 0;
    // Require all 5 questions to be answered
    private int questionsAnswered = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.getWindow().setSoftInputMode
                (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public String getAnswerOne() {
        EditText nesReleaseYear = findViewById(R.id.year_input);
        return nesReleaseYear.getText().toString();
    }

    private void calculateAnswerOne() {
        String enteredAnswerOne = getAnswerOne();
        if (enteredAnswerOne.equals("1985")) {
            score++;
            questionsAnswered++;
        } else if (enteredAnswerOne.length() > 0) {
            questionsAnswered++;
        }
    }

    private void calculateAnswerTwo() {
        RadioButton q2AnswerTwoRadio = findViewById(R.id.q2_answer_two);
        boolean q2AnswerTwoSelected = q2AnswerTwoRadio.isChecked();
        RadioButton q2AnswerOneRadio = findViewById(R.id.q2_answer_one);
        boolean q2AnswerOneSelected = q2AnswerOneRadio.isChecked();
        RadioButton q2AnswerThreeRadio = findViewById(R.id.q2_answer_three);
        boolean q2AnswerThreeSelected = q2AnswerThreeRadio.isChecked();
        RadioButton q2AnswerFourRadio = findViewById(R.id.q2_answer_four);
        boolean q2AnswerFourSelected = q2AnswerFourRadio.isChecked();
        RadioButton q2AnswerFiveRadio = findViewById(R.id.q2_answer_five);
        boolean q2AnswerFiveSelected = q2AnswerFiveRadio.isChecked();
        if (q2AnswerTwoSelected) {
            score++;
            questionsAnswered++;

        } else if (q2AnswerOneSelected ||
                q2AnswerThreeSelected ||
                q2AnswerFourSelected ||
                q2AnswerFiveSelected) {
            questionsAnswered++;
        }
    }

    private void calculateAnswerThree() {
        CheckBox answerOneCheckbox = findViewById(R.id.q3_answer_one);
        boolean isCheckboxOneSelected = answerOneCheckbox.isChecked();

        CheckBox answerTwoCheckbox = findViewById(R.id.q3_answer_two);
        boolean isCheckboxTwoSelected = answerTwoCheckbox.isChecked();

        CheckBox answerThreeCheckbox = findViewById(R.id.q3_answer_three);
        boolean isCheckboxThreeSelected = answerThreeCheckbox.isChecked();

        CheckBox answerFourCheckbox = findViewById(R.id.q3_answer_four);
        boolean isCheckboxFourSelected = answerFourCheckbox.isChecked();

        CheckBox answerFiveCheckbox = findViewById(R.id.q3_answer_five);
        boolean isCheckboxFiveSelected = answerFiveCheckbox.isChecked();

        if (isCheckboxTwoSelected && isCheckboxFiveSelected) {
            score++;
            questionsAnswered++;
            answerOneCheckbox.setChecked(false);

        } else if (isCheckboxOneSelected ||
                isCheckboxTwoSelected ||
                isCheckboxThreeSelected ||
                isCheckboxFourSelected ||
                isCheckboxFiveSelected) {
            questionsAnswered++;
        }
    }

    private void calculateAnswerFour() {
        RadioButton q4AnswerOneRadio = findViewById(R.id.q4_answer_one);
        boolean q4AnswerOneSelected = q4AnswerOneRadio.isChecked();
        RadioButton q4AnswerTwoRadio = findViewById(R.id.q4_answer_two);
        boolean q4AnswerTwoSelected = q4AnswerTwoRadio.isChecked();
        RadioButton q4AnswerThreeRadio = findViewById(R.id.q4_answer_three);
        boolean q4AnswerThreeSelected = q4AnswerThreeRadio.isChecked();
        RadioButton q4AnswerFourRadio = findViewById(R.id.q4_answer_four);
        boolean q4AnswerFourSelected = q4AnswerFourRadio.isChecked();
        RadioButton q4AnswerFiveRadio = findViewById(R.id.q4_answer_five);
        boolean q4AnswerFiveSelected = q4AnswerFiveRadio.isChecked();
        if (q4AnswerFiveSelected) {
            score++;
            questionsAnswered++;
        } else if (q4AnswerOneSelected ||
                q4AnswerTwoSelected ||
                q4AnswerThreeSelected ||
                q4AnswerFourSelected) {
            questionsAnswered++;
        }
    }

    private String getAnswerFive() {
        EditText zeldaLevels = findViewById(R.id.level_num_input);
        return zeldaLevels.getText().toString();
    }

    private void calculateAnswerFive() {
        String enteredAnswerFive = getAnswerFive();
        if (enteredAnswerFive.equals("9") ||
                enteredAnswerFive.equals("09")) {
            score++;
            questionsAnswered++;
        } else if (enteredAnswerFive.length() > 0) {
            questionsAnswered++;
        }
    }

    // Total up score
    private void calculateAnswersAll() {
        calculateAnswerOne();
        calculateAnswerTwo();
        calculateAnswerThree();
        calculateAnswerFour();
        calculateAnswerFive();
    }

    public void submitAnswers(View view) {
        calculateAnswersAll();

        // Pop-up that shows score results
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.results_title);
        alertDialogBuilder.setCancelable(false);

        // Restart quiz button from results dialog
        alertDialogBuilder.setPositiveButton(R.string.results_btn_restart, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                resetProgress();
                clearAnswers();
            }
        });

        // Answer Results
        if (questionsAnswered == 5 && score == 5) {
            alertDialogBuilder.setMessage(getString(R.string.results_all_correct));
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        } else if (questionsAnswered == 5) {
            alertDialogBuilder.setMessage(getString(R.string.results_incorrect, score));
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        } else {
            // Custom Toast warning for incomplete form
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast, null);

            ImageView toastIcon = layout.findViewById(R.id.toast_icon);
            toastIcon.setImageResource(R.drawable.error_icon);
            TextView toastText = layout.findViewById(R.id.toast_text);

            toastText.setText(getString(R.string.toast_incomplete, questionsAnswered));

            Toast toastCustom = new Toast(getApplicationContext());
            toastCustom.setDuration(Toast.LENGTH_LONG);
            toastCustom.setView(layout);
            toastCustom.show();

            // Clear progress so score/questions answered count wont double
            // after each submit
            resetProgress();
        }
    }

    // Reset the score and count after submit
    private void resetProgress() {
        score = 0;
        questionsAnswered = 0;
    }

    // Reset all answer fields from dialog
    private void clearAnswers() {
        EditText resetAnswerOne = findViewById(R.id.year_input);
        resetAnswerOne.setText("");

        RadioGroup resetAnswerTwo = findViewById(R.id.q2_group);
        resetAnswerTwo.clearCheck();

        // Clear each checkbox state
        CheckBox resetCheckOne = findViewById(R.id.q3_answer_one);
        resetCheckOne.setChecked(false);
        CheckBox resetCheckTwo = findViewById(R.id.q3_answer_two);
        resetCheckTwo.setChecked(false);
        CheckBox resetCheckThree = findViewById(R.id.q3_answer_three);
        resetCheckThree.setChecked(false);
        CheckBox resetCheckFour = findViewById(R.id.q3_answer_four);
        resetCheckFour.setChecked(false);
        CheckBox resetCheckFive = findViewById(R.id.q3_answer_five);
        resetCheckFive.setChecked(false);

        RadioGroup resetAnswerFour = findViewById(R.id.q4_group);
        resetAnswerFour.clearCheck();

        EditText resetAnswerFive = findViewById(R.id.level_num_input);
        resetAnswerFive.setText("");
    }
}
// TODO: Idea - Make submit button only visible if all questions are filled out
